# ansible-aws

## [Table of Contents](#table-of-contents)

- [Motivation](#motivation)
- [Build status of continuous integration](#build-status-of-continuous-integration)
- [Code style](#code-style)
- [Screenshots](#screenshots)
- [Tech/framework used](#tech-framework-used)
- [Requirements](#requirements)
- [Example Playbook](#example-playbook)
- [Project variables](#project-variables)
  * [Inventory `hosts` file](#inventory-hosts-file)
  * [group_vars file](#group_vars-file)
  * [host_vars file](#host_vars-file)
  * [Ansible config file](#ansible-config-file)
- [How to use?](#how-to-use)
  * [Install `git`](#install-git)
  * [Configure `git` locally](#configure-git-locally)
  * [Setup local ssh keys to connect to GitLab](#setup-local-ssh-keys-to-connect-to-gitlab)
  * [Create `git` directory, `git clone` and run Ansible playbook to configure AWS resources](#create-git-directory-git-clone-and-run-ansible-playbook-to-configure-aws-resources)
- [API Reference](#api-reference)
- [Tests](#tests)
- [Notes](#notes)
- [How this repository was created](#how-this-repository-was-created)
- [Author Informaion](#author-information)
- [How to contribute](#how-to-contribute)
- [About](#about)

This repository deploys a single simple instance of the the Amazon Web Services (AWS) EC2 web service (Elastic Compute Cloud), using the [Ansible infrastructure orchestration tool](https://www.ansible.com/).

## [Motivation](#motivation)

The benefits of a cloud-based infrastructure compared to physical hardware are documented ([1](https://sysgen.ca/cloud-vs-in-house-servers/), [2](http://highscalability.com/blog/2010/7/8/cloud-aws-infrastructure-vs-physical-infrastructure.html), [3](https://www.infoworld.com/article/3220669/iaas/what-is-iaas-the-modern-datacenter-platform.html), [4](https://www.forbes.com/sites/janakirammsv/2018/03/04/how-modern-infrastructure-and-machine-intelligence-will-disrupt-the-industry/#7f684b4a732c)). Within the providers of cloud infrastructure services, Amazon Web Services (AWS) is one of the most widely adopted solutions ([1](https://www.zdnet.com/article/cloud-providers-ranking-2018-how-aws-microsoft-google-cloud-platform-ibm-cloud-oracle-alibaba-stack/), [2](https://www.forbes.com/sites/bobevans1/2017/11/07/the-top-5-cloud-computing-vendors-1-microsoft-2-amazon-3-ibm-4-salesforce-5-sap/#467996b86f2e), [3](http://www.cloudpro.co.uk/leadership/cloud-essentials/5801/best-cloud-hosting)). AWS is thus used in the current repository, for deploying a simple AWS-based infrastructure of 2 (i.e. 2 cloud-based servers), solely due to its popularity.

While AWS has and supports a mature command line interface (CLI)-based tool to manage its cloud services, an [infrastructure orchestration tool](https://devopscube.com/devops-tools-for-infrastructure-automation/) was strongly desired. Of the [various options available](https://stackify.com/best-cloud-tools-infrastructure-automation/), [Ansible](https://www.ansible.com/overview/it-automation) was chosen for the [following reasons](https://searchitoperations.techtarget.com/definition/Ansible)
- it is agentless
- it supports the provisioning of [several cloud-based infrastructures, including AWS](https://www.ansible.com/integrations/cloud)
- based on [Python](https://www.python.org/), making for it to be [easily readable](https://quintagroup.com/cms/python/ansible)

This repository will perform the following
- create AWS IAM user(s), assign them to IAM groups and generate [AWS access credentials](https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html) (AWS ACCESS KEY and AWS SECRET ACCESS KEY) for each IAM user created
- create AWS EC2 access key(s), to be used with EC2 instances that will also be created
  - one EC2 key will be craeted per EC2 instance, allowing access to that instance
- create AWS EC2 security group and instance(s)
  - one EC2 security group will be created per EC2 instance
  - each EC2 instance will be assigned to one EC2 security group
  - each instance's details (listed next) will be added to a static Ansible inventory `hosts` file, with a
    - [host alias](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#hosts-and-groups)
    - public IP address
    - path to Python interpreter
    - path to Private key to allow for keypair-based SSH access by the default EC2 user created for the instance ([default user name is based on AMI ID](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html))
  - for each EC2 instance created
    - an Ansible `group_vars` and `host_vars` file will be created
    - 3 [CloudWatch](https://aws.amazon.com/cloudwatch/) alarms will be set up
      - recover the instance automatically if it goes down
      - alarm when CPU use reaches a specified threshold
      - disk space use reaches a specified threshold
    - generate SSH keypairs for non-EC2 user(s) to be created
- configure EC2 instances, by Ansible inventory group
  - update and upgrade all installed packages
  - install various packages, including
    - [Docker](https://www.docker.com/)
    - [`fail2ban`](https://www.fail2ban.org/wiki/index.php/Main_Page)
    - Ubuntu [Automatic Updates](https://help.ubuntu.com/lts/serverguide/automatic-updates.html.en) and [Unattended Upgrades](https://help.ubuntu.com/community/AutomaticSecurityUpdates)
    - [`ufw` firewall management in Linux](https://docs.ansible.com/ansible/2.5/modules/ufw_module.html#ufw-manage-firewall-with-ufw)
  - create non-EC2 user(s)
    - assign one SSH public key for each non-EC2 user created on the EC2 instance
    - assign user(s) to group(s)
    - grant passwordless `sudo` access to each user
    - customize SSH configuration
- poweroff and delete the EC2 instance
  - shut down the instance (power off)
  - detach the EC2 instance from its EC2 security group
  - delete the EC2 instance
  - if spepcified by the `delete_ec2_groups` variable, delete the EC2 security group

## [Build status of continuous integration](#build-status-of-continuous-integration)

None. May be done in the future via [Travis CI](https://travis-ci.org/).

## [Code style](#code-style)

None

## [Screenshots](#screenshots)

N/A

## [Tech/framework used](#tech-framework-used)

[Ansible](https://www.ansible.com/about-us)

## [Requirements](#requirements)

1. PC ([desktop](https://en.wikipedia.org/wiki/Desktop_computer) or [laptop](https://en.wikipedia.org/wiki/Laptop) personal computer)/virtual-machine with [Ubuntu desktop](https://www.ubuntu.com/desktop) installed
   1. this version of Ubuntu can be
        - the [Gnome](https://www.gnome.org/)-based flavour of Ubuntu, or
        - one of the [other official flavours](https://www.ubuntu.com/download/desktop)
            - [Ubuntu](https://www.ubuntu.com/download/desktop)
            - [XUbuntu](https://xubuntu.org/download/), or
            - [LUbuntu](https://lubuntu.net/downloads/)
   2. Ansible will be
      - installed on this desktop
          - it will act as the Ansible controller
      - used to configure software on this desktop
          - it will generally __also__ be treated as the Ansible target
2. Internet connection to PC running Ubuntu
3. Since the Ansible project will be created inside a [Python virtual environment](https://docs.python-guide.org/dev/virtualenvs/) on the local machine, install `virtualenv` package on the Ansible controller
   ```
   $ sudo apt-get install -y virtualenv
   ```
4. AWS account ([step 1.1 from here](https://docs.aws.amazon.com/polly/latest/dg/setting-up.html)) with `root` user account (not [IAM](https://aws.amazon.com/iam/) [user](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)) [access keys generated](https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html)
   - AWS `root` account access keys will only be used to create IAM user(s) and will not be used further, favoured by IAM access keys [per AWS best practices](https://aws.amazon.com/blogs/security/wheres-my-secret-access-key/)

## [Example Playbook](#example-playbook)

```
---
hosts: localhost
connection: local
gather_facts: false

roles:
  - create-ec2
```

## [Project variables](#project-variables)

### [Inventory `hosts` file](#inventory-hosts-file)
File  `ansible-aws/inventories/staging/hosts`

 - no changes are required here

### [group_vars file](#group_vars-file)
File `ansible-aws/inventories/production/group_vars/all/all.yml`

 - no changes are required here

### [host_vars file](#host_vars-file)
File `ansible-aws/inventories/production/host_vars/my_local_machine`

 - no changes are required here

### [Ansible config file](#ansible-config-file)
File `ansible-aws/ansible.cfg`

 - `vault_password_file`
   - if `vault_pass.txt` is not located in the same folder as `ansible.cfg`, then the value of this variable should be set to the [absolute path](https://www.linux.com/blog/absolute-path-vs-relative-path-linuxunix) of the `vault_pass.txt` file
   - if `vault_pass.txt` is located in the same folder as `ansible.cfg`, then no changes are required here

## [How to use?](#how-to-use)

### [Install `git`](#install-git)
Install `git` [using](https://www.liquidweb.com/kb/install-git-ubuntu-16-04-lts/)

```
$ sudo apt-get update
$ sudo apt-get install git-core
````

### [Configure `git` locally](#how-git-was-configured-locally)

Configure `git` locally [using](https://docs.gitlab.com/ee/university/training/topics/env_setup.html) the following
```
$ git_user="edesz"
$ git_email="abcd@def.com"
$ git config --global user.name $git_user
$ git config --global user.email $git_email
```

### [Setup local ssh keys to connect to GitLab](#setup-local-ssh-keys-for-gitLab)

Create an account on [GitLab](https://about.gitlab.com/). [SSH Keys are required to access GitLab remote repositories from the server](https://docs.gitlab.com/ee/ssh/). SSH keys for GitLab were setup up locally using [these instructions](https://docs.gitlab.com/ee/ssh/README.html)

1. Specify name for key file
    ```
    $ keyname='yourkeynamehere'
    ```

2. Generate key (with no passphrase)
    ```
    $ ssh-keygen -b 2048 -t rsa -f ~/.ssh/$keyname -q -N ""
    ```
3. Copy key text to clipboard
    ```
    $ sudo apt-get install -y xclip
    $ xclip -sel clip < ~/.ssh/$keyname.pub
    ```

4. From GitLab (https://gitlab.com/profile/keys), paste key and save

5. Run `ssh-add`
    ```
    $ ssh-add ~/.ssh/$keyname
    ```
   Alternatively, [setup a ssh configuration file](https://themeteorchef.com/tutorials/setting-up-an-ssh-config-file#tmc-creating-an-ssh-config-file) [with the path the the key](https://stackoverflow.com/a/46454143).

6. [Add GitLab to known hosts file](https://serverfault.com/a/856198)
    ```
    $ ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts
    ```

7. Test if remote repository can be cloned
    ```
    $ test_path="/home/`whoami`/Downloads/jfgbdv"
    $ mkdir -p $test_path
    $ git clone git@gitlab.com:edesz/jfgbdv.git $test_path
    ```
   If the last command above (step 7.) does not return an error message, then `git` has been successfully configured locally.

### [Create `git` directory, `git clone` and run Ansible playbook to configure AWS resources](#create-git-directory-git-clone-and-run-ansible-playbook-to-configure-aws-resources)

1. Create Ansible project directory
   ```
   $ proj_path=/home/`whoami`/Downloads
   $ mkdir -p $proj_path
   ```

2. Clone `git` repository into Ansible project directory
   ```
   $ ansible_proj=$proj_path/ansible-aws
   $ git clone git@gitlab.com:edesz/ansible-aws.git $ansible_proj
   ```

3. Create an `ansible-vault` `vault_pass.txt` file with the ansible vault password - see [here (**Using Ansible Vault with a Password File**)](https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04#running-ansible-with-vault-encrypted-files)
   ```
   $ touch $ansible_proj/vault_pass.txt
   $ echo 'yourvaultpasswordhere' >> $ansible_proj/vault_pass.txt
   ```

4. Create Python virtual environment
   ```
   $ cd $ansible_proj && virtualenv .venv
   ```

5. Activate a [Python virtual environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) (`venv`), upgrade the Python package installation tool [`pip`](https://packaging.python.org/key_projects/#pip) and install the required Python packages. In this case only the [`ansible`](https://pypi.org/project/ansible/) Python package is required
   ```
   $ source .venv/bin/activate
   $ pip install --upgrade pip
   $ pip install --upgrade ansible
   ```

6. Encrypt remote server variables in `group_vars/all`, for the `staging` vault
   ```
   $ansible_proj$ ansible-vault create inventories/staging/group_vars/all/vault
   ```
   This will launch the `staging` vault file in a `vi` editor.

    a. In the `staging/group_vars/all/vault` file, enter:
    ```
    ---
    vault_ubuntu_password: <your_local_machine_password_here>
    ec2_access_key: <your_AWS_root_access_key_here>
    ec2_secret_key: <your_AWS_root_secret_access_key_here>
    vault_ssh_key_passphrase: <your_passphrase_for_EC2_ssh_key_here>
    vault_new_user_temp_password: <your_IAM_user_password_here>
    ```
    
    b. Save and exit `vi` editor with <kbd>:wq</kbd> + <kbd>Enter</kbd>

    c. Verify that link to the `ansible-vault` password file (`vault_pass.txt`) is in `ansible.cfg` - see [here (**Reading the Password File Automatically**)](https://www.digitalocean.com/community/tutorials/how-to-use-vault-to-protect-sensitive-ansible-data-on-ubuntu-16-04#running-ansible-with-vault-encrypted-files)

7. Create`roles/create-ec2/files` to contain the `*.pem` files that will be generated by the `create-ec2` task

8. Run Ansible playbook, or use `bash` script to run Ansible playbook as explained in step 8. (if running step 8., do not need to run step 7.)
    ```
    $ansible_proj$ ansible-playbook -i inventories/staging aws_config.yml
    ```

9. (OPTIONAL) Instead of step 7, run `bash` script which runs Ansible playbook (if running step 7., do not need to run step 8.)
    ```
    $ chmod u+x ec2.sh
    $ ./ec2.sh
    ```

10. To exit from the Python venv, deactivate the `venv` with
    ```
    $ansible_proj$ deactivate
    ```
11. Backup ansible directory, excluding hidden files
    ```
    $ansible_proj$ cd ~
    $ rsync -a --exclude='.*' /home/`whoami`/Downloads/ansible-aws/ /home/`whoami`/Downloads/ansible-aws_backup
    ```

## [API Reference](#api-reference)

None

## [Tests](#tests)

None (may be done in the future via [Travis CI](https://travis-ci.org/)) - see here ([1](https://www.jeffgeerling.com/blog/testing-ansible-roles-travis-ci-github), [2](https://tasdikrahman.me/2017/04/06/Testing-your-ansible-roles-using-travis-CI/)).

## [Notes](#notes)

1. __BUG__
   - The role `ec2-authorization-keys` does not allow non-`root` users to login using their SSH key
     - since this repository uses an Ubuntu AMI, the default (`root`) user created is `ubuntu` and this user can login without SSH keys
     - this role
       - creates non-`ubuntu` (non-`root`) users and assigns them to their specified groups
       - moves their keys from the local machine (Ansible controller) to the EC2 instance
       - changes permissions of the `~/.ssh` directory and `~/.ssh/authorized_keys` file
       - grants non-`root` users paswordless `sudo` access
       - changes the SSH configuration
   - For debugging, please refer to the [links](#ansible-to-manage-ssh-user-keys) that were used to create this role
2. The following AWS-related variables are acquired programmatically, as such it is not possible to manually specify these
   - VPC ID
   - VPC Subnet ID
   - AMI ID
3. In the `create-ec2` role, the nested dict `servers` does not have a key for AMI ID since the variable `ami_details` is used to create the variable `ami_id`. This retrieved AMI ID is then used to create all EC2 instances. All EC2 instances created use the same AMI ID. This means that all instances will use the same Linux distribution. If separate AMIs (Linux distrubutions) must be used in each EC2 instance, then a new key-value pair must be entered for each server in `servers`, where the value is the AMI ID for the required distribution
4. In the `ec2-authorization-keys` and `ec2-install-docker` roles, the variable `dev_users` must match
     - the list `dev_users` in the nested dict `servers` in the `create-ec2` role
5. For demonstration, 2 of the 3 servers listed in the nested dict `servers`, in the `create-ec2` role, are assigned to the same inventory group. Both servers are configured simultaneously by targetting their group in the 2nd play of `aws_config.yml` named `Provision remote (target) EC2 instances`
6. For each created EC2 instance
   - its `group_vars` file will be empty
   - its `host_vars` file will contain a temporary ansible-vault protected password to be used by all non-`root` users created on that instance
7. If running the Ansible playbook using step 8. from the [How to use?](#create-git-directory-git-clone-and-run-ansible-playbook-to-configure-aws-resources) section, note that the `bash` script does not activate (`source .venv/bin/activate`) and deactivate (`deactivate`) the Python virtual environment. Instead, it just executes the `ansible-playbook` command. Including the activation and deactivation commands to the playbook has __not been tested__ and could lead to unexpected behaviour
   - eg. in the role `create-ec2`, `ansible-vault` is used in one of tasks in the playbook `ansible_encrypt_ec2_key.yml`, which is called by `main.yml`. The `ansible-vault` command has not been tested, inside a task, when the activation and deactivation commands are included in `ec2.sh`.
8. This repository uses a static inventory to connect to EC2 instances. Ansible offers a script for managing EC2 instances with a dynamic inventory, that can be implemented in the future. See [1](https://medium.com/happy5/aws-dynamic-inventory-and-ansible-thank-god-i-can-sleep-more-4d2aeadbc6f), [2 - YouTube video](https://www.youtube.com/watch?v=Tcu1wsX-skM), [3 - Yogesh Kumar, overview](https://www.youtube.com/watch?v=VlIl2gyF7kA).
9. The Ansible roles `create-ec2` and `delete-ec2` list the role `pre-create-ec2` as a dependency. This means that the dependent role `pre-create-ec2` should only run once, per the [Ansible documentation](https://docs.ansible.com/ansible/2.5/user_guide/playbooks_reuse_roles.html#role-dependencies). However, the role `pre-create-ec2` runs multiple times. This is a bug that has been reported [elsewhere](#ansible-miscellaneous) (see the links in link number 32.).
10. The EC2 instance names specified in `delete-ec2/defaults/main.yml` (in the variable `tag_filters_for_instances_to_delete`) and `create-ec2/defaults/main.yml` (under the key `name_of_instance` in the variable `servers`) must
    - match eachother
    - correspond to EC2 instances that have not been previously created since the `delete-ec2` role will pick up extra instance IDs. The extra IDs will correspond to previously terminated but still listed instances that can no longer be deleted.
11. After creating a new user(s) <new_user_name(s)> (eg. `www_dev`) and given them passwordless `sudo` access, with the `ec2-authorization-keys` role, but before logging in as the new user (eg. as the `www_dev` user, in the 2nd play of `aws_config.yml`) with an authorization key (not password), do the following
    - File `inventories/staging/hosts`
      - change `ansible_ssh_user`: `ubuntu` to `ansible_ssh_user`: `www_dev`
      - change `ansible_ssh_private_key_file=./roles/create-ec2/files/..._aws-private.pem` to `ansible_ssh_private_key_file=~/.ssh/www_dev_<name_of_instance>` (where `www_dev_<name_of_instance>` is the name of the private key file)
    - File `aws_config.yml`
      - change `remote_user: "{{ ec2_deploy_user }}"` to `remote_user: www_dev`
12. Although the playbook `aws_config.yml` contains 3 plays, it is perhaps more practical to have one playbook for each of
    - creating EC2 key, group, instance, cloudwatch alarms, static inventory files/folders
    - provisioning the EC2 instance
    - deleting EC2 group (optional), instance, static inventory files/folders

## [How this repository was created](#how-this-repository-was-created)

This repo was created by pushing files from an existing `~/Downloads/ansible-aws` folder [using (00:00-06:18)](https://www.youtube.com/watch?v=A5E0EbHbSjQ)
```
cd /home/`whoami`/Downloads/ansible-aws
git init
git remote add origin git@gitlab.com:$git_user/ansible-aws.git
git add .
git commit -m "Initial commit"
git push -u origin master
rm -rf .git # (OPTIONAL) this is in order to stop tracking this folder with git
```

## [Author Information](#author-information)

GitLab

- [@edesz](https://gitlab.com/edesz)

## [How to contribute](#how-to-contribute)

The best way to contribute to this project is by issuing a pull request. Thanks!

## [About](#about)

The following links were used in creating the various roles in this repository

### [AWS CLI](#aws-cli)
1. How to set up aws config file (`~/.aws/config` and `~/.aws/credentials`)
   - [1 - `~/.aws/credentials`](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html#setup-credentials-setting)
   - [2 - see sections __AWS Configuration File__ (`AWS_CONFIG_FILE`) and __AWS Credential File__ (`AWS_CREDENTIAL_FILE`)](http://www.dowdandassociates.com/blog/content/howto-install-aws-cli-security-credentials/)
2. [AWS CLI commands cheatsheet](https://github.com/toddm92/aws/wiki/AWS-CLI-Cheat-Sheet)
3. [AWS CLI lookup AMI image ID](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html)
4. [AWS CLI get subnet ID](https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-vpcs.html#description)

### [Ansible AWS EC2 keypairs](#ansible-aws-ec2-keypairs)
1. [Use Ansible to greate AWS `ec2` group and instance](https://blog.scottlowe.org/2016/10/23/managing-aws-infrastructure-ansible/)
2. [Ansible manage keypairs](https://www.packtpub.com/mapt/book/virtualization_and_cloud/9781786469199/3/ch03lvl1sec24/key-pair)
3. [How to add `ec2` keypair](https://stackoverflow.com/a/45727565/4057186)
4. How to setup `ec2` keypair, download it locally and use it
   - [1 - shows permission required](http://www.bogotobogo.com/DevOps/Ansible/Ansible-aws-creating-ec2-instance.php)
     - the line `ansible_ssh_user=ubuntu` is used since the default user created automatically for an Ubuntu-based AMI is `ubuntu` (see link #15 from [here](#ansible-and-ec2-groups-and-instances))
   - [2](https://stackoverflow.com/a/38921440/4057186)
   - [3 - shows permission required](https://just-thor.com/2017/02/how-to-connect-to-aws-ec2-instance-from-ansible/)

### [Ansible for AWS credentials](#ansible-for-aws-credentials)
1. [Provide AWS keys directly in any `ec2` module](https://stackoverflow.com/a/38714996/4057186)
2. [Get account id for user whose AWS credentials are used to call EC2 modules](https://docs.ansible.com/ansible/2.6/modules/aws_caller_facts_module.html#return-values)

### [Ansible how to specify `venv` in python interpreter](#ansible-how-to-specify-venv-in-python-interpreter)
1. [Worked - use `ansible_python_interpreter` in inventory file](https://github.com/ansible/ansible/issues/15019#issuecomment-388534905)
2. [Worked - same as above line](https://stackoverflow.com/a/41775896/4057186)

### [Table of AWS AMI IDs](#table-of-aws-ami-ids)
1. [CentOS AMI IDs](https://wiki.centos.org/Cloud/AWS)
2. [Ubuntu AMI IDs](https://askubuntu.com/a/533491/389946)

### [Ansible to list AMI images](#ansible-to-list-ami-images)
1. [Ansible `ec2_ami_facts` module](https://docs.ansible.com/ansible/2.6/modules/ec2_ami_facts_module.html#parameters)
2. [Ansible lookup AMI `image_id`](https://askubuntu.com/a/922895/389946)
4. [Use `jinja2` templating to sort AMIs by `creation date` and extract the last (latest, or most recent) AMI ID](https://github.com/ansible/ansible/issues/36829#issuecomment-389326257)

### [Ansible to list VPC images](#ansible-to-list-vpc-images)
1. use `json_query` to filter for specific subnets based on a single conditional
   - [1](https://stackoverflow.com/a/45961509/4057186)
   - [2](http://jmespath.org/examples.html#filtering-and-selecting-nested-data)
2. [use `json_query` to extract last returned subnet's VPC ID](https://stackoverflow.com/a/48631599/4057186)
3. [use `jinja2` map function to extract the first returned subnet's VPC ID](https://docs.ansible.com/ansible/2.6/modules/ec2_vpc_subnet_facts_module.html#examples)
4. [Ansible to get `subnet_id`](https://docs.ansible.com/ansible/2.6/modules/ec2_vpc_subnet_facts_module.html#examples)

### [Ansible to manipulate IAM user keys](#ansible-to-manipulate-iam-user-keys)
1. [Link 1 - retrieve `ACCESS_KEY` and `SECRET_ACCESS_KEY` from Ansible](https://stackoverflow.com/questions/44788277/retrieve-keys-value-from-registered-variable-in-ansible)
2. [Link 2 - use `json_query` to retrieve access key info ](https://stackoverflow.com/questions/44833169/ansible-json-query-is-adding-extra-characters-to-dictionary-values)

### [Ansible and EC2 groups and instances](#ansible-and-ec2-groups-and-instances)
1. [Get facts about `ec2` groups](https://docs.ansible.com/ansible/2.5/modules/ec2_group_facts_module.html#examples)
2. Add an AWS `ec2` security group with in- and outbound firewall rules
   - [1](https://www.youtube.com/watch?v=v5EGu9HKKfo)
   - [2](https://blog.scottlowe.org/2016/10/23/managing-aws-infrastructure-ansible/)
   - [3 - `ec2_group` docs](https://docs.ansible.com/ansible/2.5/modules/ec2_group_module.html#examples)
   - [4](https://github.com/ansible/ansible/issues/12677#issuecomment-296677703)
3. Get facts (`instance_id`) about `ec2` instances
   - [1](https://docs.ansible.com/ansible/2.4/ec2_instance_facts_module.html#return-values)
   - [2](https://stackoverflow.com/a/47502670/4057186)
   - [3](https://stackoverflow.com/a/39201036/4057186)
   - [4 - BEST (get `instance_id`s and append them to a list - even works for single-element list)](https://www.jeffgeerling.com/blog/2017/adding-strings-array-ansible)
4. [Filter `ec2_remote_facts` by `tag_Name_<mytaghere>`, to get facts about specific `ec2` instances](https://stackoverflow.com/a/47567719/4057186)
5. [Delete `ec2` instance by `instance_id`](https://blog.scottlowe.org/2016/10/23/managing-aws-infrastructure-ansible/)
6. Create `ec2` instances with `instance_tag` and `count_tags`
   - [1](https://stackoverflow.com/a/37707263/4057186)
   - [2](https://docs.ansible.com/ansible/latest/modules/ec2_module.html?highlight=ec2#examples)
   - [3](https://cloudership.com/blog/2016/2/14/using-ansible-to-spawn-a-vm-and-then-perform-tasks-on-it)
7. [Delete `ec2` instance by `tag_Name_<mytaghere>`](https://serverfault.com/a/865613/367989)
8. Add `ec2` instance to local inventory
   - [1](https://github.com/arbabnazar/ansible-aws-roles/blob/master/roles/ec2instance/tasks/main.yml)
   - [2](https://github.com/andrejmaya/aws-ec2-docker_ansible/blob/master/roles/ec2_basic/tasks/main.yml)
   - [3](https://github.com/ansible/ansible/issues/21468#issuecomment-280187662)
9. [Filter instances by multiple tags](https://github.com/ansible/ansible/issues/31536#issue-264282236)
10. Run tasks on EC2 instances
    - target instances by host group
      - see example [__# Launch instances, runs some tasks and then terminate them__](https://docs.ansible.com/ansible/2.6/modules/ec2_module.html#examples)
    - [target instances by instance tag](https://blog.scottlowe.org/2016/10/23/managing-aws-infrastructure-ansible/)
11. [Ansible loop over EC2 instances, created using `tagged_instances`](https://github.com/ansible/ansible/issues/13871#issuecomment-251191557)
12. [`ec2` task to create instance with volumes](https://octopus.com/blog/octopus-ansible)
13. [Remove `ec2` group](https://docs.ansible.com/ansible/2.5/modules/ec2_group_module.html#examples)
14. [Default user for Ubuntu-based AMI is `ubuntu`](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html)
15. EC2 instance too many authentication failures - [workaround is](https://forums.aws.amazon.com/thread.jspa?threadID=64456)
     - restart system or
     - delete ssh keypair, run playbook, generate ssh keypair
     - other discussions of this problem
       - [1](https://stackoverflow.com/questions/31051504/unable-to-connect-from-ec2-server)
       - [2](https://security.stackexchange.com/questions/65120/ssh-always-too-many-authentication-failures)
       - [3 - not tested this](https://stuff-things.net/2016/07/06/ssh-too-many-authentication-failures/)

### [Ansible for IAM](#ansible-for-iam)
1. [Get fields from existing IAM roles through `iam_role_facts`](https://stackoverflow.com/a/48256584/4057186)
2. [How to use `iam_role_facts` module to get list of existing IAM roles](https://docs.ansible.com/ansible/2.5/modules/iam_role_facts_module.html#examples)
3. [Ansible run `pre_tasks`](https://knpuniversity.com/screencast/ansible/pre-tasks-facts)
4. [Ansible get credentials from new `iam_user`](https://stackoverflow.com/a/45891049/4057186)
5. [Create IAM user](https://docs.ansible.com/ansible/2.6/modules/iam_user_module.html#examples)
6. Create IAM group with `managed_policy`
   - [see example __# Create a group and attach a managed policy using its ARN__](https://docs.ansible.com/ansible/latest/modules/iam_group_module.html?highlight=iam_group#examples)
7. [Ansible `iam` module](https://docs.ansible.com/ansible/2.6/modules/iam_module.html#examples)
8. [How to attach IAM user group policy from `awscli`](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html#getting-started_create-admin-group-cli)
9. Unauthorized IAM user error message
   - [1](https://github.com/joshpmcghee/parsec-terraform/issues/4)
   - [2](https://github.com/ansible/ansible/issues/22552)
10. How to create an IAM role with a policy file
    - [1 - see section __To create an instance profile and store the role in it (AWS CLI)__](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-service.html#roles-creatingrole-service-cli)
    - [2 - see 2nd example from __STEPS TO REPRODUCE__](https://github.com/ansible/ansible/issues/26023#issue-237910220)
    - [3 - see __Implementation through command line__](https://devopsideas.com/create-attach-iam-ec2-role/)
    - [4 - Yogesh Mehta tutorial on IAM user/group/role/policy management from `awscli`](https://www.youtube.com/watch?v=ppkdprBMm9c)
      - __USED THIS TO CREATE IAM ROLE WITH ANSIBLE__
        - 08:10 - for Service, he used service name as `ec2.amazonaws.com` (corresponds to __Amazon EC2__)
        - 08:17 - what the role can do (user who assumes this role) the policy - this policy itself is not mentioned in this video but is shown in the below video ([using web interface]((https://www.youtube.com/watch?v=7RXdshayP88)))
        - 09:15 - information about the role created
      - __ADDITIONAL NOTES__ assigned policy __AmazonEC2FullAccess__ to a group based on
        - 06:00 - 06:54
          - giving group permission to shutdown, restart or any kind of activity on EC2 instances
          - equivalent to my `Ensure users are assigned to groups` task, but I used a differnt policy
        - 08:00 - 10:00
          - creating a role __TestRole__ with custom trust policy `ec2-role-trust-policy.json`
    - [5 - Yogesh Mehta tutorial on IAM user/group/role/policy management from web interface](https://www.youtube.com/watch?v=7RXdshayP88)
      - __USED THIS TO CREATE IAM ROLE WITH ANSIBLE__
        - 12:14 - for Service, he used __Amazon EC2__
        - 12:25 - gave user (who assumes this role) the policy __AdministratorAccess__
        - 12:45 - information about the role created (lists service name as `ec2.amazonaws.com`)  
      - __ADDITIONAL NOTES__ assigned policy __AdministratorAccess__ to a newly created group based on
        - 09:51 - 10:56
          - add users to newly created group for which policy __AdministratorAccess__ was assigned above
        - 10:57 - 11:43
          - create role __SystemAdmin__ as role type __Amazon EC2__ with policy __AdministratorAccess__
          - in above video, he assigned policy __AmazonEC2FullAccess__ to a group
          - 11:44 - 12:51
11. Ansible vault to secure AWS credentials
    - [option 1](https://dev.iachieved.it/iachievedit/ansible-and-aws-part-3/)
    - [option 2](https://selivan.github.io/2018/01/31/ansible-dynamic-inventory-ec2-keys-in-vault.html)
    - when these approaches can be used
      - these are the preferred setup if the IAM user's credentials are known __BEFORE__ running any ansible AWS modules
    - why these approaches cannot be used in the current workflow
      - in the workflow in this repository, the user's credentials are __generated__ using an Ansible task and so this link's approach cannot be used here
12. Ansible IAM role must be run from EC2 instance
    - [1](https://serverfault.com/questions/691971/can-i-use-iam-roles-for-ansible#comment1042922_692122)
    - [2](https://serverfault.com/questions/898686/can-ansible-ec2-module-use-iam-role-instead-of-aws-access-keys#comment1161741_898686)
    - [3](https://serverfault.com/questions/898686/can-ansible-ec2-module-use-iam-role-instead-of-aws-access-keys#comment1161940_898686)
    - [4 - must remove credentials file if using role](https://serverfault.com/questions/898686/can-ansible-ec2-module-use-iam-role-instead-of-aws-access-keys#comment1161782_898686)

### [Ansible miscellaneous](#ansible-miscellaneous)
1. [Add line to file at beginning](https://gist.github.com/muloka/7687250)
2. [Check if file contains string](https://stackoverflow.com/a/38462337/4057186)
3. [How to encrypt file with `ansible-vault`](https://stackoverflow.com/a/38462337/4057186)
4. [Why `lineinfile` and not `blockinfile` was used to add IAM user keys to `vars` file](https://github.com/ansible/ansible-modules-extras/issues/1592#issuecomment-179476153)
5. Ansible iterate inner and outer lists of dictionary (for assigning IAM users to group or groups)
   - see [__Loop 3: create personal users' directories using with_subelements__](https://chromatichq.com/blog/untangling-ansible-loops)
6. [How to include a variables file for task](https://stackoverflow.com/a/24576578/4057186)
7. [Run different Ansible roles on different hosts](https://stackoverflow.com/a/38186632/4057186)
8. [Get length of Ansible list](https://github.com/ansible/ansible/issues/9888#issue-52834910)
9. Wait until server finished booting and is accepting SSH connections
   - [Link used](https://github.com/StreisandEffect/streisand/blob/master/playbooks/roles/genesis-amazon/tasks/main.yml)
   - could not use
     - [1 - example __# Launch instances, runs some tasks
\# and then terminate them__](https://docs.ansible.com/ansible/2.6/modules/ec2_module.html#examples)
10. [Ansible `when` with `not in`](https://stackoverflow.com/a/32786381/4057186)
11. [Ansible `file` module needs `changed_when: False`](https://github.com/ansible/ansible/issues/30226#issuecomment-328952676)
12. [Ansible write raw text to file](http://perfspy.blogspot.com/2016/06/ansible-tricks.html)
13. [Ansible refresh inventory in middle of play](https://docs.ansible.com/ansible/latest/modules/meta_module.html#examples)
14. [SSH private key cannot be encrypted by Ansible vault](https://github.com/ansible/ansible/issues/22382#issue-212567702)
15. [Find file based on filetype](https://docs.ansible.com/ansible/2.5/modules/find_module.html#examples)
16. [Read contents of file](https://docs.ansible.com/ansible/2.6/plugins/lookup/file.html#examples)
17. [List in Ansible can be in bracket notation - see __Dictionaries and lists can also be represented in an abbreviated form if you really want to:__](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html#yaml-basics)
18. [Ansible filter with key-value pair using multiple values for same key](https://github.com/aws/aws-cli/issues/582#issuecomment-31789627)
19. Ansible loop over multiple tasks, or over all tasks in a playbook
    - [Link 1](https://stackoverflow.com/a/35128533/4057186)
    - [Link 2](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#loop-control)
20. [Ansible `authorized key` module needs `lookup` - cannot use `with_items`](https://serverfault.com/a/792140/367989)
21. [Ansible get hostname](https://serverfault.com/a/760878/367989)
22. [Ansible `become` for `include`](https://github.com/ansible/ansible/issues/18338#issuecomment-279734463)
23. [Ansible join 2 lists](https://stackoverflow.com/a/31045409/4057186)
24. [Ansible `loop_control` with `when`](https://github.com/ansible/ansible/issues/22426#issuecomment-285475273)
25. [Ansible find file by partial filename](http://www.mydailytutorials.com/ansible-delete-multiple-files-directories-ansible/)
26. Modify sudoers file with Ansible
    - [1](https://www.reddit.com/r/ansible/comments/5xzj8b/how_to_modify_sudoers_file_with_ansible/)
    - [2](https://stackoverflow.com/a/33362805/4057186)
    - [3](https://stackoverflow.com/q/44374427/4057186)
    - [4](https://stackoverflow.com/a/46723428/4057186)
    - [5](https://stackoverflow.com/a/37334415/4057186)
    - [6](https://askubuntu.com/a/192062/389946)
    - [7 - (USED THIS) to add a group, need `%` before group name](https://www.digitalocean.com/community/tutorials/how-to-edit-the-sudoers-file-on-ubuntu-and-centos#how-to-modify-the-sudoers-file)
27. [Get service status from Ansible](https://stackoverflow.com/a/47801057/4057186)
28. [Ansible get path to roles folder automatically from ansible variable `roles_path`](https://stackoverflow.com/a/35496603/4057186)
29. Ansible `assert`
    - [1](https://gist.github.com/halberom/62a3abb110f32960370c)
    - [2](https://docs.ansible.com/ansible/latest/modules/assert_module.html#examples)
    - [3](https://stackoverflow.com/a/29156354/4057186)
30. [Ansible role dependencies requires old format](https://github.com/ansible/ansible/issues/22169#issuecomment-364007014)
31. [Ansible use `any_errors_fatal`](https://medium.com/@abhijeet.kamble619/10-things-you-should-start-using-in-your-ansible-playbook-808daff76b65)
32. Ansible roles listed as a dependency, by multiple roles, run multiple times even though they should only run once
    - [1](https://github.com/ansible/ansible/issues/44395)
    - [2](https://stackoverflow.com/questions/32783852/ansible-best-practice-do-not-repeat-common-role)
    - [3](https://github.com/ansible/ansible/issues/5971)
33. [Ansible `shell` command with `warn=False`](https://ansibledaily.com/avoid-ansible-command-warnings/)
34. [Ansible role for `mysql` protocol for `ufw`](https://github.com/eellak/ansible/tree/master/roles/ufw)
35. Ansible to enable and restart `ufw`
    - [1](https://github.com/eellak/ansible/blob/master/roles/ufw/handlers/main.yml)
    - [2](https://docs.ansible.com/ansible/2.5/modules/ufw_module.html#examples)
36. [Ansible `no_log: True` for a role](https://github.com/ansible/proposals/issues/109#issuecomment-384098965)

### [Shell scripting](#shell-scripting)
1. [Check if file contains string](https://stackoverflow.com/a/28360230/4057186)
2. [`../` (one level up) versus `./` (current directory)](https://askubuntu.com/a/358637/389946)
3. [Find owner and group for file](https://unix.stackexchange.com/a/7732/255869)
4. [Find permission code for file](https://askubuntu.com/a/152003/389946)
5. [SSH key permissions should be 0400](https://stackoverflow.com/a/9270753/4057186)
6. [New user should be added to group `sudo` to act as a sudoer](https://unix.stackexchange.com/a/122197/255869)
7. [Edit `/etc/sudoers.tmp` to add group names with `sudo` access](http://mixeduperic.com/ubuntu/how-to-setup-a-user-or-group-with-sudo-privileges-on-ubuntu.html)
8. [List users with empty password](https://serverfault.com/a/240966/367989)
9. [`bash` extract specific columns from output](https://stackoverflow.com/a/39551663/4057186)
10. How to find owner and group of a directory
    - [1](https://askubuntu.com/a/175061/389946)
    - [2](https://stackoverflow.com/a/15486406/4057186)
11. [Add new user to sudoers file from `bash` terminal](https://unix.stackexchange.com/a/438503/255869)
12. [Restart `ufw` service from CLI](https://www.digitalocean.com/community/tutorials/how-to-setup-a-firewall-with-ufw-on-an-ubuntu-and-debian-cloud-server)

### [Ansible tutorials found for setting up AWS](#ansible-tutorials-found-for-setting-up-aws)

The following are full tutorials for deploying AWS with Ansible (not used here)

1. [Allocate elastic IP to EC2 instance](http://www.tothenew.com/blog/launching-and-configuring-an-aws-ec2-instance-using-ansible/)
2. [Create EC2 instance with tags and SSD volume](https://infinitypp.com/ansible/create-aws-resources-using-ansible/)
3. Ansible to configure an EC2 instance - users, groups, etc. (__GOOD__)
   - [Link 1](https://dev.iachieved.it/iachievedit/ansible-and-aws-part-4/)
   - [Link 2](https://dev.iachieved.it/iachievedit/ansible-and-aws-part-5/)

### [Ansible dynamic inventory for EC2](#ansible-dynamic-inventory-for-ec2)

The following are links for using a dynamic AWS EC2 inventory with Ansible (not used here)

1. [Thorough, but very (maybe too?) advanced, walkthrough of Ansible with AWS EC2 dynamic inventory](https://www.devon.nl/en/ansible-dynamic-inventory-for-ec2/)
2. [Specify custom paths for AWS EC2 dynamic inventory script files `ec2.py` and `ec2.ini`](https://stackoverflow.com/a/42419881/4057186)
3. [Yogesh Mehta tutorial on AWS EC2 dynamic inventory - 14:14-22:26](https://www.youtube.com/watch?v=VlIl2gyF7kA)
4. [Beginner's tutorial - good](https://aws.amazon.com/blogs/apn/getting-started-with-ansible-and-dynamic-amazon-ec2-inventory-management/)

### [Ansible to manage SSH user keys](#ansible-to-manage-ssh-user-keys)
1. [How to generate `.pem` file](https://gist.github.com/mingfang/4aba327add0807fa5e7f#gistcomment-2387583)
2. [`id_rsa.pem` file can be created by renaming `id_rsa` privatekey file](https://linuxaws.wordpress.com/2017/07/17/how-to-generate-pem-file-to-ssh-the-server-without-password-in-linux/)
3. Permissions for SSH keypair
   - [Link 1](https://stackoverflow.com/a/44830865/4057186)
   - [Link 2](https://stackoverflow.com/a/32082442/4057186)
4. From `.pem`-`.pub` keypair, send `.pub` key to EC2 instance
   - [Link 1](https://serverfault.com/a/715383/367989)
   - [Link 2](https://stackoverflow.com/q/45620777/4057186)
5. [Grant an SSH user passwordless `sudo` access to EC2 instance](https://askubuntu.com/a/192062/389946)
6. [Full guide on how to manage AWS keys with `bash` - `.pem` is not required](https://stackoverflow.com/a/25967554/4057186)
7. [Full guide on how to manage SSH keys with Ansible](https://www.cyberciti.biz/faq/how-to-upload-ssh-public-key-to-as-authorized_key-using-ansible/)
8. [System users/groups versus normal users/groups](https://askubuntu.com/a/524010/389946)
9. [Ubuntu restart and get status of SSH service](https://www.cyberciti.biz/faq/howto-start-stop-ssh-server/)

### [Install `docker` and `docker-machine` on Ubuntu](#install-docker-and-docker-machine-on-ubuntu)
1. Need users added to `docker` group to avoid typing `sudo`
   - [1](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04#step-2-%E2%80%94-executing-the-docker-command-without-sudo-(optional))
   - [2](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
2. Ansible tasks to add users to `docker` group
   - [1](https://github.com/nickjj/ansible-docker)
   - [2](https://github.com/ppc64le/ciaas/blob/master/roles/host_setup/tasks/main.yml)
3. Install `docker-machine`
   - [official instructtions](https://docs.docker.com/machine/install-machine/#install-machine-directly)
   - Ansible roles
     - [1 - used this for `docker`](https://github.com/Oefenweb/ansible-docker-machine/blob/master/tasks/install.yml)
     - [2](https://github.com/giovtorres/ansible-role-docker-machine/blob/master/tasks/main.yml)
     - [3](https://github.com/andrewrothstein/ansible-docker-machine/blob/master/tasks/main.yml)
     - [4 - used this for `docker-machine`](https://github.com/booz-allen-hamilton/ansible-role-docker/blob/master/tasks/docker-machine.yml)
     - [5 - use ansible variables instead of `uname` commands](https://github.com/giovtorres/ansible-role-docker-machine/blob/master/tasks/main.yml)

### [Ansible CloudWatch alarms](#ansible-cloudwatch-alarms)
1. Links to the 3 `ec2_metric_alarm` tasks used here
   - [Alarm to check disk use](https://github.com/smiller171/Ansible-CloudWatch/blob/master/roles/alarm/tasks/add-alarm.yaml)
     - [how to specify `topic` variable](https://github.com/smiller171/Ansible-CloudWatch)
   - [Alarm for CPU use](https://docs.ansible.com/ansible/2.5/modules/ec2_metric_alarm_module.html#examples)
   - [Alarm to auto-recover instance](https://github.com/StreisandEffect/streisand/blob/master/playbooks/roles/genesis-amazon/tasks/main.yml)
     - also shown [here](https://github.com/ansible/ansible-modules-core/issues/1527)
2. [How to configure CloudWatch from AWS web interface - not used](https://n2ws.com/blog/security-compliance/amazon-inspector-cloudtrail)
3. How to write AWS ARN
   - [AWS documentation with full format explained](https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html)
     - see 1st example under __Amazon Simple Notification Service (Amazon SNS)__
   - [Ansible documentation for `sns_topic` with 2 sample ARNs](https://docs.ansible.com/ansible/2.5/modules/sns_topic_module.html#parameters)
4. Unit for CloudWatch `StatusCheckFailed` alarm is `Count`
   - see section __Amazon EC2 Metrics__ and search for [`StatusCheckFailed`](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/ec2-metricscollected.html)